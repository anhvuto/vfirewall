const listen_port = 3000;
var express = require("express"),
    app = express(),
    bodyParser = require("body-parser"),
    cors = require('cors');

app.use(cors())
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var routes = require("./routes"); //importing route
routes(app); //register the route
app.listen(listen_port);
console.log("Server start on port: " + listen_port);
