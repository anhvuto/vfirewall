module.exports = function (app) {
  let vfirewall = require("./vfirewall");
  app.route("/getRules").get(vfirewall.getRules);
  app.route("/removeRule").post(vfirewall.removeRule);
};
