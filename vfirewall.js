var nfq = require('nfqueue');
var IPv4 = require('pcap/decode/ipv4');
var netmask = require('netmask').Netmask
var cmd = require('node-cmd');

// Firewall rules
var rules = [
	{
		src: '113.173.47.164/32',
		dst: '8.8.8.8',
		serviceStart: 0,
		serviceEnd: 65535,
		action: nfq.NF_ACCEPT
	}
];

var count = 0;
var NFQueueID = process.argv.slice(2);
console.log('NFQueueID: ', NFQueueID);

nfq.createQueueHandler(Number(NFQueueID), 65535, function (nfpacket) {
	var packet = new IPv4().decode(nfpacket.payload, 0);
	var verdict = nfq.NF_DROP;

	// We're only firewalling for TCP (protocol : 6)
	if (packet.protocol == 6) {
		//console.log(JSON.stringify(packet));
		_handlePacket(packet);
		verdict = nfq.NF_DROP;
	}
	else {
		// ACCEPT anything that isn't TCP
		verdict = nfq.NF_ACCEPT;
	}
	nfpacket.setVerdict(verdict);
	count++;
});

function _handlePacket(packet) {
	var srcArr = packet.saddr.addr;
	var src = `${srcArr[0]}.${srcArr[1]}.${srcArr[2]}.${srcArr[3]}`;
	var dstArr = packet.daddr.addr;
	var dst = `${dstArr[0]}.${dstArr[1]}.${dstArr[2]}.${dstArr[3]}`;
	/**
	 * 
	 *  "saddr": {
        "addr": [
            127,
            119,
            144,
            3
        ]
    },
    "daddr": {
        "addr": [
            103,
            127,
            197,
            249
        ]
    }
	 * 
	 */

	if (src.lenght === 0) {
		return;
	}
	var index = rules.findIndex(el => el.src === src);
	if (index > -1) { // found
		return;
	}
	rules.push({
		src: src,
		dst: dst,
		serviceStart: 0,
		serviceEnd: 65535,
		action: nfq.NF_DROP
	});
	var command = `ethtool -U ens160 flow-type tcp4 src-ip ${src} dst-ip ${dst} action -1 loc ${rules.length - 1}`;
	console.log(command);
	cmd.run(command);
}


async function getRules(req, res) {
	return res.json(rules)
}

async function removeRule(req, res) {
	let src = req.body.src;
	if (!src) { // null or empty 
		return res.status(400).send({ error: 'src not set' });
	}
	let index = rules.findIndex(item => item.src === src);
	if (index === -1) {
		return res.status(400).send({ error: 'src not found' })
	}
	rules.splice(index, 1);
	return res.json({
		success: true,
		rules: rules
	})
}


module.exports = {
	getRules: getRules,
	removeRule: removeRule
}