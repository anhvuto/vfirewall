var nfq = require('nfqueue');
var IPv4 = require('pcap/decode/ipv4');
var netmask = require('netmask').Netmask

// Delete idle sessions after 10 minutes
var idleTimeout = 600;

// Firewall rules
var rules = [
{
	src: '192.168.0.0/16',
	dst: '8.8.8.8',
	serviceStart: 0,
	serviceEnd: 65535,
	action: nfq.NF_DROP
},
{
	src: '192.168.0.0/16',
	dst: '0.0.0.0/0',
	serviceStart: 0,
	serviceEnd: 65535,
	action: nfq.NF_ACCEPT
}
];

// Our session state object
var sessions = {};

// The different states of a session as we see it, not exactly RFC compliant.
const sessionState = {
	SYNSENT : 0,
	ESTABLISHED : 1,
	CLOSING: 2,
	CLOSED: 3
};

// Add the necessary masks so we can match them later
for(var i in rules) {
	rules[i].srcMask = new netmask(rules[i].src);
	rules[i].dstMask = new netmask(rules[i].dst);
}

nfq.createQueueHandler(30, 65535, function(nfpacket) {
	var packet = new IPv4().decode(nfpacket.payload, 0);
	var verdict = nfq.NF_DROP;

	// We're only firewalling for TCP (protocol : 6)
	if(packet.protocol == 6) {
		/*
			Check for existing session first
		*/
			var key = create_key(packet.daddr, packet.payload.dport, packet.saddr, packet.payload.sport);
			if(typeof sessions[key] !== 'undefined') {
				verdict = nfq.NF_ACCEPT;

				if(packet.payload.flags.syn && packet.payload.flags.ack) {
					update_session(key, sessionState.ESTABLISHED);
				}
				else if(packet.payload.flags.fin) {
					if(sessions[key].state == sessionState.CLOSING) {
						update_session(key, sessionState.CLOSED);
					}
					else {
						update_session(key, sessionState.CLOSING);
					}
				}
				else if(packet.payload.flags.rst) {
					update_session(key, sessionState.CLOSED);
				}

				sessions[key].idle = 0;
			}
			else {
				/*
				Couldn't find existing session, run through rulebase.
				*/
				var key = create_key(packet.saddr, packet.payload.sport, packet.daddr, packet.payload.dport);
				for(var i in rules) {
					var rule = rules[i];

				if(rule.srcMask.contains(packet.saddr) && rule.dstMask.contains(packet.daddr) && (packet.payload.dport >= rule.serviceStart && packet.payload.dport <= rule.serviceEnd)) {
					if(packet.payload.flags.syn) {
						if(rule.action != nfq.NF_DROP) {
							update_session(key, sessionState.SYNSENT);
						}
					}
					verdict = rule.action;
					// Break loop early as no further rule processing is required
					break;
				}
			}
		}
	}
	else {
		// ACCEPT anything that isn't TCP
		verdict = nfq.NF_ACCEPT;
	}
	nfpacket.setVerdict(verdict);
});

var create_key = function(srcip, srcport, dstip, dstport) {
	return [srcip, srcport, dstip, dstport].sort();
}

var age_sessions = function(sessions) {
	for(var i in sessions) {
		sessions[i].idle++;
		if(sessions[i].idle > idleTimeout || sessions[i].state == sessionState.CLOSED) {
			console.log("Reaping " + i);
			delete(sessions[i]);
		}
	}
	setTimeout(function(){age_sessions(sessions)}, 1000);
}
setTimeout(function(){age_sessions(sessions)}, 1000);

var update_session = function(key, state) {
	if(typeof sessions[key] === 'undefined') {
		sessions[key] = {};
	}
	sessions[key].state = state;
	sessions[key].idle = 0;
}

